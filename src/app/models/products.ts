import { Category } from "./category";
export interface Product {
     _id: string;
     name: string;
     price: number;
     category: Category;
     product_image: string;
}