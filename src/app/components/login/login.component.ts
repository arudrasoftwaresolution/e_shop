import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error:string;
  success:string;
  form: HTMLFormElement;
  constructor(private userService: UserService, private router:Router) { }

  ngOnInit() {
  }

  login(event : Event){
    event.preventDefault();
    this.form = <HTMLFormElement>event.target;
    this.readFromValues();
  }

  navigateToHomePage(){
    this.router.navigate(['']);
  }

  readFromValues(){
    let email = (<HTMLInputElement>this.form.elements.namedItem('email')).value;
    let password = (<HTMLInputElement>this.form.elements.namedItem('password')).value;

    let credentials = {
      email,password
    }
    this.userService.login(credentials).subscribe((res)=> {
      this.success=res.message;
      this.error=undefined;
      this.navigateToHomePage();
    },(errResponse:HttpErrorResponse) => {
      this.error = errResponse.error.error.message;
      this.success=undefined;
    })
  }
}
