import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  numberOfItems:number = 0;
  constructor(private _cartService: CartService) { }

  ngOnInit() {
    this._cartService.cartObservable.subscribe({
      next : (cart)=>{
        console.log(cart);
        this.numberOfItems = Object.keys(cart).length;
      },
      error : (response :HttpErrorResponse) => {
        console.log(response);
      }
    })
  }

}
