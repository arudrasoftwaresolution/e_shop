import { Product } from './../../models/products';
import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  products:Product[] = [];
  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this.collectProducts();
  }

  collectProducts(){
    this._productService.getAllProducts().subscribe(
      (products) => {
        this.products = products;
        console.log(this.products);
      },
      (response:HttpErrorResponse) => {
        console.log(response);
      }
    );
  }

}
