import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Product } from 'src/app/models/products';
import { ProductService } from 'src/app/services/product/product.service';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

interface cartItem{
  product: Product,
  quantity: number
}


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cart:any;
  cartItems:cartItem[] = [];
  total = 0;
  constructor(private cartService:CartService, private productServie:ProductService) { }

  ngOnInit():void {
    this.subscribeCart();
  }

  subscribeCart(){
    this.cartService.cartObservable.subscribe(
      {
        next: (cart) => {
          this.cartItems = [];
          let observables=[];
          for(let id in cart){ 
            observables.push(
              this.productServie.getProductById(id)
              .pipe(
                map(product => {
                  this.total += (product.price * cart[id]);
                  let item:cartItem = {
                      product : product,
                      quantity: cart[id]
                  }
                  return item;
                })
              )
            ) 
          }
          forkJoin(observables).subscribe({
            next : (cartItems:cartItem[]) =>{
              this.cartItems = cartItems;
            }
          })
        },
        error: (err:HttpErrorResponse) => {
          console.log(err);
        }
      }
    )
  }
}
