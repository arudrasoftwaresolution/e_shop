import { Category } from './../../models/category';
import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category/category.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  categoires:Category[];
  constructor(private _categoryService:CategoryService) { }

  ngOnInit() : void{
    this.collectAllCategories();
  }

  collectAllCategories(){
    this._categoryService.getAllCategories().subscribe(
      (categories)=>{
        this.categoires=categories;
        console.log(this.categoires);
      },
      (response:HttpErrorResponse)=>{
        console.log(response);
      }
    );
  }


  //button

  btnCategorySelected(category_id:string){
    console.log(category_id);
  }
}
