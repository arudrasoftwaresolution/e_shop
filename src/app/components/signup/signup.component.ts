import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  success:string;
  error:string;
  constructor(private userService:UserService, private router: Router) { }

  ngOnInit() {
  }


  navigateToLoginPage(){
    this.router.navigateByUrl("/login");
  }

  readValuesFromForm(form: HTMLFormElement){
    let name=(<HTMLInputElement>form.elements.namedItem('name')).value;
    let email=(<HTMLInputElement>form.elements.namedItem('email')).value;
    let password=(<HTMLInputElement>form.elements.namedItem('password')).value;
    let phone=(<HTMLInputElement>form.elements.namedItem('phone')).value;

    let user: User = {
      name,
      email,
      password,
      phone
    };

    return user;
  }


  signup(event : Event){
    event.preventDefault();
    let form = <HTMLFormElement>event.target;
    let user = this.readValuesFromForm(form);
    this.createUser(user,form);
  }

  createUser(user: User, form: HTMLFormElement){
    this.userService.signup(user).subscribe((res)=>{
      this.success=res.message;
      form.reset();
      this.navigateToLoginPage();
      this.error=undefined;
    },(errResponse:HttpErrorResponse)=>{
      this.error=errResponse.error.error.message;
      this.success=undefined;
    })
  }
}
