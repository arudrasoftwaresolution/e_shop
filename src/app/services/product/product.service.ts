import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UserService } from '../user/user.service';
import { Product } from 'src/app/models/products';


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  allProductUrl:string="http://localhost/api/products";
  constructor(private _http:HttpClient, private _service: UserService) { }

  getAllProducts(){
    return this._http.get(this.allProductUrl, 
      {
        headers:{
          'authorization': this._service.getToken()
        }
      }).pipe(
        map((result : {count : number, products: Product[]}) => {
          return result.products;
        })
      )
  }

  getProductById(id :string){
    return this._http.get(`${this.allProductUrl}/${id}`, 
      {
        headers:{
          'authorization': this._service.getToken()
        }
      }).pipe(
        map((result) => {
          return <Product> result;
        })
      )
  }
}
