import { User } from 'src/app/models/user';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userSignupUrl="http://localhost/api/users/signup";
  private userLoginUrl="http://localhost/api/users/login";
  constructor(private _http: HttpClient) { }
  
  private saveTokenLocalStorage(token :string){
    return localStorage.setItem('token',"Bearer "+ token);
  }


  getToken(){
    return localStorage.getItem('token') ? localStorage.getItem('token') : " ";
  }


  signup(user:User){
    return this._http.post(this.userSignupUrl,user).pipe(
      map(result=>{
        return <{message:string}>result;
      })
    );
  }

  login(credentials:{email:string,password:string}){
    return this._http.post(this.userLoginUrl,credentials).pipe(
      map((result : loginResponse) => {
        this.saveTokenLocalStorage(result.token);
        return result;
      })
    )
  }
}


interface loginResponse{
  token:string,
  message:string
}
