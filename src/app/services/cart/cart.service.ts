import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/products';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cart = {};
  private _cartObservable : BehaviorSubject<Object>;
  constructor() {
    if(!this.isCartExist())
      localStorage.setItem('cart',JSON.stringify(this.cart));

    this.readCartDataFromLocalStorage();
    this._cartObservable = new BehaviorSubject(this.cart);
  }

  getQuantity(product : Product){
    return this.cart[product._id] ? +this.cart[product._id] : 0;
  }

  readCartDataFromLocalStorage(){
    this.cart = JSON.parse(localStorage.getItem('cart'));
  }

  writeCartDataToLocalStorage(){
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

  setQuantity(product:Product, quantity:number){
    if(quantity < 1){
      delete this.cart[product._id];
    }else{
      this.cart[product._id]=quantity;
    }
    this.writeCartDataToLocalStorage();
    this._cartObservable.next(this.cart);
  }

  isCartExist(){
    if(localStorage.getItem('cart')){
      return true;
    }else{
      return false;
    }
  }

  get cartObservable(){
    return this._cartObservable;
  }

  addToCart(product : Product){
    let quantity= this.cart[product._id];
    if(quantity){
      this.cart[product._id]= quantity+1;
    }
    else{
      this.cart[product._id] = 1;
    }

    this._cartObservable.next(this.cart);
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

  removeFromCart(product : Product){

  }
}
