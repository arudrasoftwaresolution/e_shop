import { Category } from './../../models/category';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  getURL="http://localhost/api/categories";
  constructor(private _http: HttpClient) { }

  getAllCategories(){
    return this._http.get(this.getURL).pipe(
      map(result=>{
        return <Category[]>result['categories'];
      })
    )
  }
}
